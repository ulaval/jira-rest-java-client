package com.atlassian.jira.rest.client.api.domain;

public class JiraGroup {

    private final String group;
    private final String username;

    public JiraGroup(String group, String username) {
        super();
        this.group = group;
        this.username = username;
    }

    public String getGroup() {
        return group;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public String toString() {
        return "group : " + group + ", username : " + username;
    }
}