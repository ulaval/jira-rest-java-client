package com.atlassian.jira.rest.client.api;

import com.atlassian.jira.rest.client.api.domain.Team;
import com.atlassian.util.concurrent.Promise;

public interface TeamRestClient {

	Promise<Team> getTeamById(Long id);
	
	Promise<Team> getTeamByUsername(String username);

}
