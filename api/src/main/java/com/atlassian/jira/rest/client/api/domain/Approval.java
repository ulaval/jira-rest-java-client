package com.atlassian.jira.rest.client.api.domain;

import com.google.common.base.Objects;

public class Approval {
	
	private BasicUser user;
	private String status;
	private Long workedSeconds;
	private Long submittedSeconds;
	private Long requiredSeconds;
	private Action action;
	
	public Approval(BasicUser user, String status, Long workedSeconds, Long submittedSeconds, Long requiredSeconds, Action action){
		this.user = user;
		this.status = status;
		this.workedSeconds = workedSeconds;
		this.submittedSeconds = submittedSeconds;
		this.requiredSeconds = requiredSeconds;
		this.action = action;
	}

	public BasicUser getUser() {
		return user;
	}

	public String getStatus() {
		return status;
	}

	public Long getWorkedSeconds() {
		return workedSeconds;
	}

	public Long getSubmittedSeconds() {
		return submittedSeconds;
	}

	public Long getRequiredSeconds() {
		return requiredSeconds;
	}

	public Action getAction() {
		return action;
	}

	public String toString() {
		return Objects.toStringHelper(this).
				add("user", user).
				add("status", status).
				add("workedSeconds", workedSeconds).
				add("submittedSeconds", submittedSeconds).
				add("requiredSeconds", requiredSeconds).
				add("action", action).
				toString();
	}

	public boolean equals(Object obj) {
		if (obj instanceof Approval) {
			Approval that = (Approval) obj;
			return  Objects.equal(this.user, that.user)
					&& Objects.equal(this.status, that.status)
					&& Objects.equal(this.workedSeconds, that.workedSeconds)
					&& Objects.equal(this.submittedSeconds, that.submittedSeconds)
					&& Objects.equal(this.requiredSeconds, that.requiredSeconds)
			&&Objects.equal(this.action, that.action);
		}
		return false;
	}

	public int hashCode() {
		return Objects.hashCode(user, status, workedSeconds, submittedSeconds, requiredSeconds, action);
	}
}
