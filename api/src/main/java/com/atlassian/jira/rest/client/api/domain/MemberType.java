package com.atlassian.jira.rest.client.api.domain;

public enum MemberType {
	
	USER,
	GROUP
}
