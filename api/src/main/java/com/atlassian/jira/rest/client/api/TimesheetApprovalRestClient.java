package com.atlassian.jira.rest.client.api;

import com.atlassian.jira.rest.client.api.domain.TimesheetApproval;
import com.atlassian.util.concurrent.Promise;

public interface TimesheetApprovalRestClient {

    Promise<TimesheetApproval> getTimesheetApprovalByPeriod(String period);

}
