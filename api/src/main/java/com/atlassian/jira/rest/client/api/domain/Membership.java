package com.atlassian.jira.rest.client.api.domain;

import org.joda.time.DateTime;

import com.atlassian.jira.rest.client.api.IdentifiableEntity;
import com.google.common.base.Objects;

public class Membership implements IdentifiableEntity<Long> {
	
	private Long id;
	
	private MembershipRole role;

	private DateTime dateFrom;
	
	private DateTime dateTo;
	
	private Long availability;
	
	private Long teamMemberId;
	
	private Long teamId;
	
	private String status;
	
	public Membership(Long id, MembershipRole role, DateTime dateFrom, DateTime dateTo, Long availability, Long teamMemberId,Long teamId, String status){
		this.id = id;
		this.role = role;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.availability = availability;
		this.teamMemberId = teamMemberId;
		this.teamId = teamId;
		this.status = status;
	}

	@Override
	public Long getId() {
		return id;
	}

	public MembershipRole getRole() {
		return role;
	}

	public DateTime getDateFrom() {
		return dateFrom;
	}

	public DateTime getDateTo() {
		return dateTo;
	}

	public Long getAvailability() {
		return availability;
	}

	public Long getTeamMemberId() {
		return teamMemberId;
	}

	public String getStatus() {
		return status;
	}

	public Long getTeamId() {
		return teamId;
	}

	public void setTeamId(Long teamId) {
		this.teamId = teamId;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setRole(MembershipRole role) {
		this.role = role;
	}

	public void setDateFrom(DateTime dateFrom) {
		this.dateFrom = dateFrom;
	}

	public void setDateTo(DateTime dateTo) {
		this.dateTo = dateTo;
	}

	public void setAvailability(Long availability) {
		this.availability = availability;
	}

	public void setTeamMemberId(Long teamMemberId) {
		this.teamMemberId = teamMemberId;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String toString() {
		return Objects.toStringHelper(this).
				add("id", id).
				add("role", role).
				add("dateFrom", dateFrom).
				add("dateTo", dateTo).
				add("availability", availability).
				add("teamMemberId", teamMemberId).
				add("status", status).
				toString();
	}

	public boolean equals(Object obj) {
		if (obj instanceof Membership) {
			Membership that = (Membership) obj;
			return this.id.equals(that.id)
					&& Objects.equal(this.role, that.role)
					&& Objects.equal(this.dateFrom, that.dateFrom)
					&& Objects.equal(this.dateTo, that.dateTo)
					&& Objects.equal(this.availability, that.availability)
					&& Objects.equal(this.teamMemberId, that.teamMemberId)
					&& Objects.equal(this.status, that.status);
		}
		return false;
	}

	public int hashCode() {
		return Objects.hashCode(id, role, teamMemberId, dateFrom, dateTo, availability, status);
	}
}
