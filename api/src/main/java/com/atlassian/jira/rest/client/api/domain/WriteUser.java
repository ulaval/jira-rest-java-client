package com.atlassian.jira.rest.client.api.domain;

public class WriteUser extends BasicUser {

    private final String emailAddress;

    public WriteUser(String name, String displayName, String emailAddress) {
        super(null, name, displayName);
        this.emailAddress = emailAddress;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    @Override
    public String toString() {
        return "name : " + getName() + ", displayName : " + getDisplayName() + ", emailAdress : " + getEmailAddress();
    }
}