package com.atlassian.jira.rest.client.api.domain;

import org.joda.time.DateTime;

import com.google.common.base.Objects;

public class Period {
	
	private String periodString;

	private String periodView;
	
	private DateTime dateFrom;
	
	private DateTime dateTo;
	
	public Period(String periodString, String periodView, DateTime dateFrom, DateTime dateTo){
		this.periodString = periodString;
		this.periodView = periodView;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
	}

	public String getPeriodString() {
		return periodString;
	}

	public String getPeriodView() {
		return periodView;
	}

	public DateTime getDateFrom() {
		return dateFrom;
	}

	public DateTime getDateTo() {
		return dateTo;
	}
	
	public void setPeriodString(String periodString) {
		this.periodString = periodString;
	}

	public void setPeriodView(String periodView) {
		this.periodView = periodView;
	}

	public void setDateFrom(DateTime dateFrom) {
		this.dateFrom = dateFrom;
	}

	public void setDateTo(DateTime dateTo) {
		this.dateTo = dateTo;
	}

	public String toString() {
		return Objects.toStringHelper(this).
				add("periodString", periodString).
				add("periodView", periodView).
				add("dateFrom", dateFrom).
				add("dateTo", dateTo).
				toString();
	}

	public boolean equals(Object obj) {
		if (obj instanceof Period) {
			Period that = (Period) obj;
			return Objects.equal(this.periodString, that.periodString)
					&& Objects.equal(this.periodView, that.periodView)
					&& Objects.equal(this.dateFrom, that.dateFrom)
					&& Objects.equal(this.dateTo, that.dateTo);
		}
		return false;
	}

	public int hashCode() {
		return Objects.hashCode(periodString, periodView, dateFrom, dateTo);
	}

}
