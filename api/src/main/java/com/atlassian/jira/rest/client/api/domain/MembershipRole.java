package com.atlassian.jira.rest.client.api.domain;

import com.atlassian.jira.rest.client.api.IdentifiableEntity;
import com.google.common.base.Objects;

public class MembershipRole implements IdentifiableEntity<Long> {

	private Long id;
	
	private String name;
	
	private boolean isDefault;
	
	public MembershipRole(Long id, String name, boolean isDefault){
		this.id = id;
		this.name = name;
		this.isDefault = isDefault;
	}
	
	@Override
	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public boolean isDefault() {
		return isDefault;
	}
	
	public String toString() {
		return Objects.toStringHelper(this).
				add("id", id).
				add("name", name).
				add("isDefault", isDefault).
				toString();
	}

	public boolean equals(Object obj) {
		if (obj instanceof MembershipRole) {
			MembershipRole that = (MembershipRole) obj;
			return this.id.equals(that.id)
					&& Objects.equal(this.name, that.name)
					&& Objects.equal(this.isDefault, that.isDefault);
		}
		return false;
	}

	public int hashCode() {
		return Objects.hashCode(id, name, isDefault);
	}

}
