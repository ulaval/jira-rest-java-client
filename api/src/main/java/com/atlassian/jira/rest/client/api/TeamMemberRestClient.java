package com.atlassian.jira.rest.client.api;

import com.atlassian.jira.rest.client.api.domain.TeamMember;
import com.atlassian.util.concurrent.Promise;

public interface TeamMemberRestClient {

	Promise<Iterable<TeamMember>> getTeamMembersByTeamId(Long teamId);
}
