package com.atlassian.jira.rest.client.api.domain;

import java.util.Date;

import com.google.common.base.Objects;

public class Action {

	private String name;
	private BasicUser actor;
	
	private Date created;
	
	

	public Action(String name, BasicUser actor, Date created) {
		super();
		this.name = name;
		this.actor = actor;
		this.created = created;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BasicUser getActor() {
		return actor;
	}

	public void setActor(BasicUser actor) {
		this.actor = actor;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this).
				add("name", name).
				add("basicUser", actor).
				toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Action) {
			Action that = (Action) obj;
			return  Objects.equal(this.name, that.name)
					&& Objects.equal(this.created, that.created);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(name, actor, created);
	}
}
