package com.atlassian.jira.rest.client.api;

import com.atlassian.jira.rest.client.api.domain.JiraGroup;
import com.atlassian.util.concurrent.Promise;

public interface GroupRestClient {

    /**
     * Ajoute un usager Jira à un groupe
     * @param jiraGroup
     * @return
     */
    public Promise<Void> addUserToGroup(JiraGroup jiraGroup) throws RestClientException;

    /**
     * Retire un usager d'un groupe Jira
     * @param jiraGroup
     * @return
     */
    public Promise<Void> removeUserFromGroup(JiraGroup jiraGroup) throws RestClientException;
}
