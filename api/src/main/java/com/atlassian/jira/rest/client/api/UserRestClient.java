/*
 * Copyright (C) 2010 Atlassian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.jira.rest.client.api;

import com.atlassian.jira.rest.client.api.domain.User;
import com.atlassian.jira.rest.client.api.domain.WriteUser;
import com.atlassian.util.concurrent.Promise;

import java.net.URI;

/**
 * The com.atlassian.jira.rest.client.api handling user resources.
 *
 * @since v0.1
 */
public interface UserRestClient {

    /**
     * Retrieves detailed information about selected user.
     * Try to use {@link #getUser(URI)} instead as that method is more RESTful (well connected)
     *
     * @param username
     *            JIRA username/login
     * @param typeUsagerJira
     *            : le type d'idul avec lequel vous travaillez, TEMPO ou JIRA
     *
     * @return complete information about given user
     * @throws RestClientException
     *             in case of problems (connectivity, malformed messages, etc.)
     */
    Promise<User> getUser(String username, TypeUsagerJira typeUsagerJira);

    /**
     * Retrieves detailed information about selected user.
     * Try to use {@link #getUser(URI)} instead as that method is more RESTful (well connected)
     *
     * @param username
     *            JIRA username/login
     *
     * @return complete information about given user
     * @throws RestClientException
     *             in case of problems (connectivity, malformed messages, etc.)
     */
    Promise<User> getUser(String username);

    /**
     * Retrieves detailed information about selected user.
     * This method is preferred over {@link #getUser(String)} as com.atlassian.jira.rest.it's more RESTful (well connected)
     *
     * @param userUri
     *            URI of user resource
     * @return complete information about given user
     * @throws RestClientException
     *             in case of problems (connectivity, malformed messages, etc.)
     */
    Promise<User> getUser(URI userUri);

    /**
     * Création d'un usager JIRA
     *
     * @param user
     *            {@link WriteUser}
     * @return {@link Promise}
     * @throws Exception
     */
    Promise<Void> createUser(WriteUser user) throws RestClientException;

    /**
     * Mise à jour d'un usager JIRA
     *
     * @param user
     *            {@link WriteUser}
     * @return {@link Promise}
     * @throws Exception
     */
    Promise<Void> updateUser(WriteUser user) throws RestClientException;

    /**
     * Suppression d'un usager JIRA
     *
     * @param username
     * @return
     * @throws RestClientException
     */
    Promise<Void> removeUser(String username) throws RestClientException;

    /**
     * Fusion de deux usagers.
     * L'ancien idul est remplacé par le nouveau.
     * 
     * @param ancienIdul
     * @param user
     * @return
     * @throws RestClientException
     */
    Promise<Void> fusionnerUser(String ancienIdul, WriteUser user) throws RestClientException;

}