package com.atlassian.jira.rest.client.api.domain;

import java.net.URI;
import java.util.Map;

import com.google.common.base.Objects;

public class Member {
	
	public static String S16_16 = "16x16";
	public static String S48_48 = "48x48";
	
	private String name;
	
	private Long teamMemberId;
	
	private MemberType type;
	
	private Map<String, URI> avatarUris;
	
	private boolean activeInJira;
	
	private String displayName;
	
	public Member(String name, Long teamMemberId, MemberType type, Map<String, URI> avatarUris, boolean activeInJira, String displayName){
		this.name = name;
		this.teamMemberId = teamMemberId;
		this.type = type;
		this.avatarUris = avatarUris;
		this.displayName = displayName;
		this.activeInJira = activeInJira;
	}

	public Long getTeamMemberId() {
		return teamMemberId;
	}

	public MemberType getType() {
		return type;
	}

	public Map<String, URI> getAvatarUris() {
		return avatarUris;
	}

	public boolean isActiveInJira() {
		return activeInJira;
	}

	public String getDisplayName() {
		return displayName;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setTeamMemberId(Long teamMemberId) {
		this.teamMemberId = teamMemberId;
	}

	public void setType(MemberType type) {
		this.type = type;
	}

	public void setAvatarUris(Map<String, URI> avatarUris) {
		this.avatarUris = avatarUris;
	}

	public void setActiveInJira(boolean activeInJira) {
		this.activeInJira = activeInJira;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String toString() {
		return Objects.toStringHelper(this).
				add("name", name).
				add("teamMemberId", teamMemberId).
				add("type", type).
				add("avatarUris", avatarUris).
				add("activeInJira", activeInJira).
				add("displayName", displayName).
				toString();
	}

	public boolean equals(Object obj) {
		if (obj instanceof Member) {
			Member that = (Member) obj;
			return  Objects.equal(this.name, that.name)
					&& Objects.equal(this.teamMemberId, that.teamMemberId)
					&& Objects.equal(this.type, that.type)
					&& Objects.equal(this.avatarUris, that.avatarUris)
					&& Objects.equal(this.displayName, that.displayName);
		}
		return false;
	}

	public int hashCode() {
		return Objects.hashCode(name, teamMemberId, type, avatarUris, displayName);
	}

}
