package com.atlassian.jira.rest.client.api.domain;

import com.atlassian.jira.rest.client.api.IdentifiableEntity;
import com.google.common.base.Objects;

public class TeamMember implements IdentifiableEntity<Long> {
	
	private Long id;

	private Member member;
	
	private Membership membership;
	
	private boolean showDeactivate;
	
	public TeamMember(Long id, Member member, Membership membership, boolean showDeactivate){
		this.id = id;
		this.member = member;
		this.membership = membership;
		this.showDeactivate = showDeactivate;
	}
	
	@Override
	public Long getId() {
		return id;
	}

	public Member getMember() {
		return member;
	}

	public Membership getMembership() {
		return membership;
	}

	public boolean isShowDeactivate() {
		return showDeactivate;
	}
	
	public String toString() {
		return Objects.toStringHelper(this).
				add("id", id).
				add("member", member).
				add("membership", membership).
				add("showDeactivate", showDeactivate).
				toString();
	}

	public boolean equals(Object obj) {
		if (obj instanceof TeamMember) {
			TeamMember that = (TeamMember) obj;
			return this.id.equals(that.id)
					&& Objects.equal(this.member, that.member)
					&& Objects.equal(this.membership, that.membership)
					&& this.showDeactivate == that.showDeactivate;
		}
		return false;
	}

	public int hashCode() {
		return Objects.hashCode(id, member, membership, showDeactivate);
	}

}
