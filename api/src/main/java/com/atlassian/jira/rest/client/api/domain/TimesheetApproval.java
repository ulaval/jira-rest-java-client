package com.atlassian.jira.rest.client.api.domain;

import java.util.List;

import com.google.common.base.Objects;

public class TimesheetApproval  {

	private Period period;
	
	private List<Approval> approvals;
	
	public TimesheetApproval(Period period, List<Approval> approvals){
		this.period = period;
		this.approvals = approvals;
	}
	
	public Period getPeriod() {
		return period;
	}
	
	public void setPeriod(Period period) {
		this.period = period;
	}

	public List<Approval> getApprovals() {
		return approvals;
	}

	public void setApprovals(List<Approval> approvals) {
		this.approvals = approvals;
	}

	public String toString() {
		return Objects.toStringHelper(this).
				add("period", period).
				add("approvals", approvals).
				toString();
	}

	public boolean equals(Object obj) {
		if (obj instanceof TimesheetApproval) {
			TimesheetApproval that = (TimesheetApproval) obj;
			return Objects.equal(this.period, that.period)
					&& Objects.equal(this.approvals, that.approvals);
		}
		return false;
	}

	public int hashCode() {
		return Objects.hashCode(period, approvals);
	}

}
