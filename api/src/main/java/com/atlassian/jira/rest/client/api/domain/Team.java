package com.atlassian.jira.rest.client.api.domain;

import com.google.common.base.Objects;

public class Team {
	
	private Long id;
	private String name;
	private String summary;
	private String lead;
	private TeamUser leadUser;
	
	public Team(Long id, String name, String summary, String lead, TeamUser leadUser){
		this.id = id;
		this.name = name;
		this.summary = summary;
		this.lead = lead;
		this.leadUser = leadUser;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getLead() {
		return lead;
	}

	public void setLead(String lead) {
		this.lead = lead;
	}

	public TeamUser getLeadUser() {
		return leadUser;
	}

	public void setLeadUser(TeamUser leadUser) {
		this.leadUser = leadUser;
	}
	
	public String toString() {
		return Objects.toStringHelper(this).
				add("id", id).
				add("name", name).
				add("summary", summary).
				add("lead", lead).
				add("leadUser", leadUser).
				toString();
	}

	public boolean equals(Object obj) {
		if (obj instanceof Team) {
			Team that = (Team) obj;
			return Objects.equal(this.id, that.id)
					&& Objects.equal(this.name, that.name)
					&& Objects.equal(this.summary, that.summary)
					&& Objects.equal(this.lead, that.lead)
					&& Objects.equal(this.leadUser, that.leadUser);
		}
		return false;
	}

	public int hashCode() {
		return Objects.hashCode(id, name, summary, lead, leadUser);
	}

}
