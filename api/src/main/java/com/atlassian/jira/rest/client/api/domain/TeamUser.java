package com.atlassian.jira.rest.client.api.domain;

import java.net.URI;
import java.util.Map;

import com.google.common.base.Objects;

public class TeamUser {
	
	private String name;
	
	private String key;
	
	private Map<String, URI> avatarUris;
	
	private boolean jiraUser;
	
	private String displayName;
	
	public TeamUser(String name, String key, Map<String, URI> avatarUris, boolean jiraUser, String displayName){
		this.name = name;
		this.key = key;
		this.avatarUris = avatarUris;
		this.jiraUser = jiraUser;
		this.displayName = displayName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Map<String, URI> getAvatarUris() {
		return avatarUris;
	}

	public void setAvatarUris(Map<String, URI> avatarUris) {
		this.avatarUris = avatarUris;
	}

	public boolean isJiraUser() {
		return jiraUser;
	}

	public void setJiraUser(boolean jiraUser) {
		this.jiraUser = jiraUser;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String toString() {
		return Objects.toStringHelper(this).
				add("key", key).
				add("name", name).
				add("avatarUris", avatarUris).
				add("jiraUser", jiraUser).
				add("displayName", displayName).
				toString();
	}

	public boolean equals(Object obj) {
		if (obj instanceof TeamUser) {
			TeamUser that = (TeamUser) obj;
			return Objects.equal(this.key, that.key)
					&& Objects.equal(this.name, that.name)
					&& Objects.equal(this.avatarUris, that.avatarUris)
					&& Objects.equal(this.jiraUser, that.jiraUser)
					&& Objects.equal(this.displayName, that.displayName);
		}
		return false;
	}

	public int hashCode() {
		return Objects.hashCode(key, name, avatarUris, jiraUser, displayName);
	}
	
}
