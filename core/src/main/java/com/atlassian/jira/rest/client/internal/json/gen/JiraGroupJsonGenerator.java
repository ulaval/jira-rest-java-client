package com.atlassian.jira.rest.client.internal.json.gen;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.atlassian.jira.rest.client.api.domain.JiraGroup;

public class JiraGroupJsonGenerator implements JsonGenerator<JiraGroup> {

    @Override
    public JSONObject generate(JiraGroup bean) throws JSONException {
        return new JSONObject().put("name", bean.getUsername());
    }
}