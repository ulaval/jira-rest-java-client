package com.atlassian.jira.rest.client.internal.json;


import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.atlassian.jira.rest.client.api.domain.Action;
import com.atlassian.jira.rest.client.api.domain.Approval;
import com.atlassian.jira.rest.client.api.domain.BasicUser;

public class ApprovalJsonParser implements JsonObjectParser<Approval> {

	@Override
	public Approval parse(JSONObject json) throws JSONException {
		
		final BasicUser user = JsonParseUtil.parseBasicUser(json.getJSONObject("user"));
		
		final String status = json.optString("status");
		
		final Long workedSeconds = json.optLong("workedSeconds");
		
		final Long submittedSeconds = json.optLong("submittedSeconds");
		
		final Long requiredSeconds = json.optLong("requiredSeconds");
		
		Action action;
		try {
			action = new ActionJsonParser().parse(json);
		} catch (Exception e) {
			action  = null; 
		}
		
		return new Approval(user, status, workedSeconds, submittedSeconds, requiredSeconds, action);
	}
}
