package com.atlassian.jira.rest.client.internal.json;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.joda.time.DateTime;

import com.atlassian.jira.rest.client.api.domain.Membership;
import com.atlassian.jira.rest.client.api.domain.MembershipRole;

public class MembershipJsonParser implements JsonObjectParser<Membership> {

	@Override
	public Membership parse(JSONObject json) throws JSONException {
		
		final Long id = json.optLong("id");
		
		final MembershipRole role = new MembershipRoleJsonParser().parse(json.getJSONObject("role"));

		final DateTime dateFrom = JsonParseUtil.parseOptionalDateTime(json, "dateFrom");
		final DateTime dateTo = JsonParseUtil.parseOptionalDateTime(json, "dateTo");
		final Long availability = json.optLong("availability");
		final Long teamMemberId = json.optLong("teamMemberId");
		final Long teamId = json.getLong("teamId");
		final String status = json.optString("status");
		
		return new Membership(id, role, dateFrom, dateTo, availability, teamMemberId, teamId, status);
	}

}
