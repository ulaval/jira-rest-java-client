package com.atlassian.jira.rest.client.internal.json.gen;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.atlassian.jira.rest.client.api.domain.WriteUser;

public class WriteUserGenerator implements JsonGenerator<WriteUser> {

    @Override
    public JSONObject generate(WriteUser bean) throws JSONException {
        return new JSONObject()
                .put("name", bean.getName())
                .put("displayName", bean.getDisplayName())
                .put("emailAddress", bean.getEmailAddress());
    }
}