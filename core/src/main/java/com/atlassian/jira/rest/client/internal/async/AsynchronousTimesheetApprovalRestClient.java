package com.atlassian.jira.rest.client.internal.async;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import com.atlassian.httpclient.api.HttpClient;
import com.atlassian.jira.rest.client.api.TimesheetApprovalRestClient;
import com.atlassian.jira.rest.client.api.domain.TimesheetApproval;
import com.atlassian.jira.rest.client.internal.json.TimeSheetApprovalJsonParser;
import com.atlassian.util.concurrent.Promise;

public class AsynchronousTimesheetApprovalRestClient extends
        AbstractAsynchronousRestClient implements TimesheetApprovalRestClient {

    private static final String TIMESHEET_APPROVAL_URI_PREFIX = "timesheet-approval";
    private final TimeSheetApprovalJsonParser timesheetApprovalJsonParser = new TimeSheetApprovalJsonParser();

    private final URI baseUri;

    protected AsynchronousTimesheetApprovalRestClient(final URI baseUri, HttpClient client) {
        super(client);
        this.baseUri = baseUri;
    }

    @Override
    public Promise<TimesheetApproval> getTimesheetApprovalByPeriod(String period) {
        final URI userUri = UriBuilder.fromUri(baseUri).path(TIMESHEET_APPROVAL_URI_PREFIX)
                .queryParam("period", period).build();
        return getAndParse(userUri, timesheetApprovalJsonParser);
    }

}
