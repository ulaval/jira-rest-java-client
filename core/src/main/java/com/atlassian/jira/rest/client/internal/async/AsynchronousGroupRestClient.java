package com.atlassian.jira.rest.client.internal.async;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import com.atlassian.httpclient.api.HttpClient;
import com.atlassian.jira.rest.client.api.GroupRestClient;
import com.atlassian.jira.rest.client.api.RestClientException;
import com.atlassian.jira.rest.client.api.domain.JiraGroup;
import com.atlassian.jira.rest.client.internal.json.gen.JiraGroupJsonGenerator;
import com.atlassian.util.concurrent.Promise;

public class AsynchronousGroupRestClient extends AbstractAsynchronousRestClient implements GroupRestClient {

    private static final String GROUP_URI_PREFIX = "group";
    private static final String USER_URI_PREFIX = "user";

    private final URI baseUri;
    private final JiraGroupJsonGenerator jiraGroupJsonGenerator = new JiraGroupJsonGenerator();

    protected AsynchronousGroupRestClient(URI baseUri, HttpClient client) {
        super(client);
        this.baseUri = baseUri;
    }

    @Override
    public Promise<Void> addUserToGroup(JiraGroup jiraGroup) throws RestClientException {

        try {
            final URI uri = UriBuilder.fromUri(baseUri).path(GROUP_URI_PREFIX).path(USER_URI_PREFIX).queryParam("groupname", jiraGroup.getGroup()).build();
            return post(uri, jiraGroup, jiraGroupJsonGenerator);
        } catch (RestClientException e) {
            throw e;
        }
    }

    @Override
    public Promise<Void> removeUserFromGroup(JiraGroup jiraGroup) throws RestClientException {

        try {
            final URI uri = UriBuilder.fromUri(baseUri)
                    .path(GROUP_URI_PREFIX)
                    .path(USER_URI_PREFIX)
                    .queryParam("groupname", jiraGroup.getGroup())
                    .queryParam("username", jiraGroup.getUsername())
                    .build();

            return delete(uri);
        } catch (RestClientException e) {
            throw e;
        }
    }
}