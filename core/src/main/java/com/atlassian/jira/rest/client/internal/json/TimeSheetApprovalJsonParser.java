package com.atlassian.jira.rest.client.internal.json;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.atlassian.jira.rest.client.api.domain.Approval;
import com.atlassian.jira.rest.client.api.domain.Period;
import com.atlassian.jira.rest.client.api.domain.TimesheetApproval;

public class TimeSheetApprovalJsonParser implements JsonObjectParser<TimesheetApproval> {

	@Override
	public TimesheetApproval parse(JSONObject json) throws JSONException {
		
		final Period period = new PeriodJsonParser().parse(json.optJSONObject("period"));
		final List<Approval> listApproval = new ArrayList<Approval>();
		
		ApprovalJsonParser approvalJsonParser = new ApprovalJsonParser();
		JSONArray approvals = json.getJSONArray("approvals");
		for(int i = 0 ; i < approvals.length(); i++) {
			listApproval.add(approvalJsonParser.parse(approvals.getJSONObject(i)));
		}
	
		return new TimesheetApproval(period, listApproval);
	}
}
