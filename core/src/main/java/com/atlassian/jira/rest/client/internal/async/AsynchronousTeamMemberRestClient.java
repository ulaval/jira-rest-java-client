package com.atlassian.jira.rest.client.internal.async;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import com.atlassian.httpclient.api.HttpClient;
import com.atlassian.jira.rest.client.api.TeamMemberRestClient;
import com.atlassian.jira.rest.client.api.domain.TeamMember;
import com.atlassian.jira.rest.client.internal.json.GenericJsonArrayParser;
import com.atlassian.jira.rest.client.internal.json.TeamMemberJsonParser;
import com.atlassian.util.concurrent.Promise;

public class AsynchronousTeamMemberRestClient extends AbstractAsynchronousRestClient implements TeamMemberRestClient {
	
	private final TeamMemberJsonParser teamMemberJsonParser = new TeamMemberJsonParser();
	private final GenericJsonArrayParser<TeamMember> teamMembersJsonParser = GenericJsonArrayParser.create(teamMemberJsonParser);

	private final URI baseUri;

	protected AsynchronousTeamMemberRestClient(URI baseUri, HttpClient client) {
		super(client);
		
		this.baseUri = baseUri;
	}

	@Override
	public Promise<Iterable<TeamMember>> getTeamMembersByTeamId(Long teamId) {
		
		final URI uri = UriBuilder.fromUri(baseUri).path("team").path(teamId.toString()).path("member").build();
		return getAndParse(uri, teamMembersJsonParser);
	}

}
