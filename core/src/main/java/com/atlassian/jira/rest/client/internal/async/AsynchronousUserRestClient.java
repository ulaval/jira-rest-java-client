/*
 * Copyright (C) 2012 Atlassian
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.jira.rest.client.internal.async;

import com.atlassian.httpclient.api.HttpClient;
import com.atlassian.jira.rest.client.api.RestClientException;
import com.atlassian.jira.rest.client.api.TypeUsagerJira;
import com.atlassian.jira.rest.client.api.UserRestClient;
import com.atlassian.jira.rest.client.api.domain.User;
import com.atlassian.jira.rest.client.api.domain.WriteUser;
import com.atlassian.jira.rest.client.internal.json.UserJsonParser;
import com.atlassian.jira.rest.client.internal.json.gen.WriteUserGenerator;
import com.atlassian.util.concurrent.Promise;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

/**
 * Asynchronous implementation of UserRestClient.
 *
 * @since v2.0
 */
public class AsynchronousUserRestClient extends AbstractAsynchronousRestClient implements UserRestClient {

    private static final String USER_URI_PREFIX = "user";
    private final UserJsonParser userJsonParser = new UserJsonParser();
    private final WriteUserGenerator writeUserGenerator = new WriteUserGenerator();

    private final URI baseUri;

    public AsynchronousUserRestClient(final URI baseUri, final HttpClient client) {
        super(client);
        this.baseUri = baseUri;
    }

    @Override
    public Promise<User> getUser(final String username, final TypeUsagerJira typeUsagerJira) {

        if (TypeUsagerJira.JIRA == typeUsagerJira) {
            final URI userUri = UriBuilder.fromUri(baseUri).path(USER_URI_PREFIX).queryParam("username", username).queryParam("expand", "groups").build();
            return getUser(userUri);

        } else if (TypeUsagerJira.TEMPO == typeUsagerJira) {
            final URI userUri = UriBuilder.fromUri(baseUri).path(USER_URI_PREFIX).queryParam("key", username).queryParam("expand", "groups").build();
            return getUser(userUri);

        } else {
            return null;
        }
    }

    @Override
    public Promise<User> getUser(final URI userUri) {
        return getAndParse(userUri, userJsonParser);
    }

    @Override
    public Promise<User> getUser(String username) {
        return getUser(username, TypeUsagerJira.TEMPO);
    }

    @Override
    public Promise<Void> createUser(WriteUser user) throws RestClientException {
        return post(UriBuilder.fromUri(baseUri).path(USER_URI_PREFIX).build(), user, writeUserGenerator);
    }

    @Override
    public Promise<Void> updateUser(WriteUser user) throws RestClientException {
        return put(UriBuilder.fromUri(baseUri).path(USER_URI_PREFIX).queryParam("username", user.getName()).build(), user, writeUserGenerator);
    }

    @Override
    public Promise<Void> removeUser(String username) throws RestClientException {
        return delete(UriBuilder.fromUri(baseUri).path(USER_URI_PREFIX).queryParam("username", username).build());
    }

    @Override
    public Promise<Void> fusionnerUser(String ancienIdul, WriteUser user) throws RestClientException {
        return put(UriBuilder.fromUri(baseUri).path(USER_URI_PREFIX).queryParam("username", ancienIdul).build(), user, writeUserGenerator);
    }
}