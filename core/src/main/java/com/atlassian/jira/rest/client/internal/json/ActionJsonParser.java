package com.atlassian.jira.rest.client.internal.json;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.atlassian.jira.rest.client.api.domain.Action;
import com.atlassian.jira.rest.client.api.domain.BasicUser;

public class ActionJsonParser implements JsonObjectParser<Action> {

	@Override
	public Action parse(JSONObject json) throws JSONException{
		
		JSONObject jsonAction = json.getJSONObject("action");
		final String name = jsonAction.optString("name");
		BasicUser actor;
		try {
			actor = JsonParseUtil.parseBasicUser(jsonAction.getJSONObject("actor"));
		} catch (Exception e1) {
			actor = null;
		}
		Date created;
		try {
			created = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").parse(jsonAction.optString("created"));
		} catch (ParseException e) {
			created = null;
		}
		return new Action(name, actor, created);
	}
}