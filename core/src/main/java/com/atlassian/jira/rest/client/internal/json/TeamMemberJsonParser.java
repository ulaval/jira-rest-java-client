package com.atlassian.jira.rest.client.internal.json;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.atlassian.jira.rest.client.api.domain.Member;
import com.atlassian.jira.rest.client.api.domain.Membership;
import com.atlassian.jira.rest.client.api.domain.TeamMember;

public class TeamMemberJsonParser implements JsonObjectParser<TeamMember> {

	@Override
	public TeamMember parse(JSONObject json) throws JSONException {
		
		final Long id = json.optLong("id");
		final Member member = new MemberJsonParser().parse(json.getJSONObject("member"));
		final Membership membership = new MembershipJsonParser().parse(json.getJSONObject("membership"));
		final boolean showDeactivate = json.getBoolean("showDeactivate");
		
		return new TeamMember(id, member, membership, showDeactivate);
	}

}
