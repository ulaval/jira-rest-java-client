package com.atlassian.jira.rest.client.internal.json;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.atlassian.jira.rest.client.api.domain.Team;
import com.atlassian.jira.rest.client.api.domain.TeamUser;

public class TeamJsonParser implements JsonObjectParser<Team> {

    @Override
    public Team parse(JSONObject json) throws JSONException {

        final Long id = json.optLong("id");
        final String name = json.optString("name");
        final String summary = json.optString("summary");
        final String lead = json.optString("lead");
        final TeamUser leadUser = new TeamUserJsonParser().parse(json.optJSONObject("leadUser"));

        return new Team(id, name, summary, lead, leadUser);
    }

}
