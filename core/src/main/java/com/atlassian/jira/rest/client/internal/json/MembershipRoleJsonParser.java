package com.atlassian.jira.rest.client.internal.json;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.atlassian.jira.rest.client.api.domain.MembershipRole;

public class MembershipRoleJsonParser implements JsonObjectParser<MembershipRole> {

	@Override
	public MembershipRole parse(JSONObject json) throws JSONException {
		final Long id = json.optLong("id");
        final String name = json.optString("name");
        final boolean isDefault = json.optBoolean("default");
		return new MembershipRole(id, name, isDefault);
	}

}
