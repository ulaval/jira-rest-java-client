package com.atlassian.jira.rest.client.internal.async;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import com.atlassian.httpclient.api.HttpClient;
import com.atlassian.jira.rest.client.api.TeamRestClient;
import com.atlassian.jira.rest.client.api.domain.Team;
import com.atlassian.jira.rest.client.internal.json.TeamJsonParser;
import com.atlassian.util.concurrent.Promise;

public class AsynchronousTeamRestClient extends AbstractAsynchronousRestClient implements TeamRestClient {

    private static final String TEAM_URI_PREFIX = "team";
    private static final String USER_TEAM_URI_PREFIX = "user/team";
    private final TeamJsonParser teamJsonParser = new TeamJsonParser();

    private final URI baseUriLatestversion;
    private final URI baseUriV1;

    protected AsynchronousTeamRestClient(URI baseUriLatestversion, URI baseURIV1, HttpClient client) {
        super(client);
        this.baseUriLatestversion = baseUriLatestversion;
        this.baseUriV1 = baseURIV1;
    }

    @Override
    public Promise<Team> getTeamById(Long id) {
        final URI uri = UriBuilder.fromUri(baseUriV1).path(TEAM_URI_PREFIX).path(id.toString()).build();
        return getAndParse(uri, teamJsonParser);
    }

    @Override
    public Promise<Team> getTeamByUsername(String username) {
        final URI uri = UriBuilder.fromUri(baseUriLatestversion).path(USER_TEAM_URI_PREFIX).queryParam("userName", username).build();
        return getAndParse(uri, teamJsonParser);
    }

}
