package com.atlassian.jira.rest.client.internal.json;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.joda.time.DateTime;

import com.atlassian.jira.rest.client.api.domain.Period;

public class PeriodJsonParser  implements JsonObjectParser<Period> {
	
	@Override
	public Period parse(JSONObject json) throws JSONException {
		
		final String periodString = json.optString("periodString");
		final String periodView = json.optString("periodView");
		final DateTime dateFrom = JsonParseUtil.parseDateTime(json, "dateFrom");
		final DateTime dateTo = JsonParseUtil.parseDateTime(json, "dateTo");
		
		return new Period(periodString, periodView, dateFrom, dateTo);
	}

}
