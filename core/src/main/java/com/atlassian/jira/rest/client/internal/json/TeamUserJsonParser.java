package com.atlassian.jira.rest.client.internal.json;

import java.net.URI;
import java.util.Iterator;
import java.util.Map;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.atlassian.jira.rest.client.api.domain.TeamUser;
import com.google.common.collect.Maps;

public class TeamUserJsonParser implements JsonObjectParser<TeamUser> {

    @Override
    public TeamUser parse(JSONObject json) throws JSONException {
        if (json != null) {

            final String name = json.getString("name");
            final String key = json.getString("key");
            final JSONObject avatarUrlsJson = json.getJSONObject("avatar");
            Map<String, URI> avatarUris = Maps.newHashMap();
            @SuppressWarnings("unchecked")
            final Iterator<String> iterator = avatarUrlsJson.keys();
            while (iterator.hasNext()) {
                final String cle = iterator.next();
                avatarUris.put(cle, JsonParseUtil.parseURI(avatarUrlsJson.getString(cle)));
            }
            final boolean jiraUser = json.getBoolean("jiraUser");
            final String displayname = json.getString("displayname");

            return new TeamUser(name, key, avatarUris, jiraUser, displayname);
        }
        return null;
    }

}
