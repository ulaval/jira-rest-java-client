package com.atlassian.jira.rest.client.internal.json;

import java.net.URI;
import java.util.Iterator;
import java.util.Map;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.atlassian.jira.rest.client.api.domain.Member;
import com.atlassian.jira.rest.client.api.domain.MemberType;
import com.google.common.collect.Maps;

public class MemberJsonParser implements JsonObjectParser<Member> {

	@Override
	public Member parse(JSONObject json) throws JSONException {
		
		String name = json.optString("name");
		Long teamMemberId = json.optLong("teamMemberId");
		MemberType type = MemberType.valueOf(json.optString("type"));
		
		Map<String, URI> avatarUris = Maps.newHashMap();
		final JSONObject avatarUrlsJson = json.getJSONObject("avatar");
		@SuppressWarnings("unchecked")
		final Iterator<String> iterator = avatarUrlsJson.keys();
		while (iterator.hasNext()) {
			final String key = iterator.next();
			avatarUris.put(key, JsonParseUtil.parseURI(avatarUrlsJson.getString(key)));
		}
		
		boolean activeInJira = json.optBoolean("activeInJira");
		String displayName = json.optString("displayname");
		
		return new Member(name, teamMemberId, type, avatarUris, activeInJira, displayName);
	}

}
