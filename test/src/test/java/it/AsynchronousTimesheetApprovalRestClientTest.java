package it;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.URI;
import java.util.concurrent.ExecutionException;

import org.junit.Test;

import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.domain.Approval;
import com.atlassian.jira.rest.client.api.domain.TimesheetApproval;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.atlassian.util.concurrent.Promise;

public class AsynchronousTimesheetApprovalRestClientTest {

    private static final URI serverUri = URI
            .create("https://jira-exp.ulaval.ca/");
//    private static final String username = "jira-api-test";
//    private static final String password = "bonjourapi";
    private static final String username = "isauc4";
private static final String password = "caramel2013!";

    @Test
    public void connectToServerTest() throws IOException {
        final JiraRestClient restClient = new AsynchronousJiraRestClientFactory()
                .createWithBasicHttpAuthentication(serverUri, username, password);

        try {

            Promise<TimesheetApproval> promise = restClient.getTimesheetApprovalRestClient().getTimesheetApprovalByPeriod("01122014");
            TimesheetApproval timesheetApproval = promise.get();

            assertNotNull(timesheetApproval);
            assertEquals("01122014", timesheetApproval.getPeriod().getPeriodString());
            
            for(Approval approval : timesheetApproval.getApprovals()) {
            	System.out.println(approval.getAction());
            }

        } catch (InterruptedException e) {
            fail();
        } catch (ExecutionException e) {
            fail();
        } finally {
            if (restClient != null) {
                restClient.close();
            }
        }
    }

}