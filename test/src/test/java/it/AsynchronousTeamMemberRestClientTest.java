package it;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.net.URI;
import java.util.concurrent.ExecutionException;

import org.junit.Test;

import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.domain.TeamMember;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.atlassian.util.concurrent.Promise;

public class AsynchronousTeamMemberRestClientTest {
	private static final URI serverUri = URI
            .create("http://jira-exp.ulaval.ca/");
    private static final String username = "jira-api-test";
    private static final String password = "bonjourapi";
 
	@Test
    public void connectToServerTest() throws IOException {
        final JiraRestClient restClient = new AsynchronousJiraRestClientFactory()
                .createWithBasicHttpAuthentication(serverUri, username, password);
 
        try {
        	
            Promise<Iterable<TeamMember>> promise = restClient.getTeamMemberRestClient().getTeamMembersByTeamId(Long.valueOf(61));
            Iterable<TeamMember> teamMembers =  promise.get();
            
            assertNotNull(teamMembers);
            for(TeamMember teamMember : teamMembers){
            	assertNotNull(teamMember.getId());
            	assertNotNull(teamMember.getMember());
            	assertEquals(Long.valueOf(61), teamMember.getMembership().getTeamId());
            	
            	System.out.println(teamMember.getMember().getName());
            	System.out.println(teamMember.getMember().getDisplayName());
            }
        } catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} finally {
            if (restClient != null) {
                restClient.close();
            }
        }
    }
}
