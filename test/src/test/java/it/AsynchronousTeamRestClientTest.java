package it;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.URI;
import java.util.concurrent.ExecutionException;

import org.junit.Test;

import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.domain.Team;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.atlassian.util.concurrent.Promise;

public class AsynchronousTeamRestClientTest {

    private static final URI serverUri = URI
            .create("http://jira-exp.ulaval.ca/");
    private static final String username = "jira-rest-api";
    private static final String password = "bonjourapi";

    @Test
    public void connectToServerTest() throws IOException {
        final JiraRestClient restClient = new AsynchronousJiraRestClientFactory()
                .createWithBasicHttpAuthentication(serverUri, username, password);

        try {

            Promise<Team> promise = restClient.getTeamRestClient().getTeamById(Long.valueOf(62));
            Team team = promise.get();

            assertNotNull(team);
            assertEquals(Long.valueOf(62), team.getId());

        } catch (InterruptedException e) {
            fail();
        } catch (ExecutionException e) {
            fail();
        } finally {
            if (restClient != null) {
                restClient.close();
            }
        }
    }

    @Test
    public void connectToTeamLatestTest() throws IOException {
        final JiraRestClient restClient = new AsynchronousJiraRestClientFactory()
                .createWithBasicHttpAuthentication(serverUri, username, password);

        try {

            Promise<Team> promise = restClient.getTeamRestClient().getTeamByUsername("jfroc8");
            Team team = promise.get();

            assertNotNull(team);
            assertEquals(Long.valueOf(103), team.getId());

        } catch (InterruptedException e) {
            fail();
        } catch (ExecutionException e) {
            fail();
        } finally {
            if (restClient != null) {
                restClient.close();
            }
        }
    }
}
