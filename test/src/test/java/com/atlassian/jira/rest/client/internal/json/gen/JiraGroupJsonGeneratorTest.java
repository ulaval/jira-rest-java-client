package com.atlassian.jira.rest.client.internal.json.gen;

import static org.junit.Assert.assertEquals;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Test;

import com.atlassian.jira.rest.client.api.domain.JiraGroup;

public class JiraGroupJsonGeneratorTest {

    @Test
    public void generatorTest() throws JSONException {

        JiraGroup jiraGroup = new JiraGroup("group", "Jacob");

        JiraGroupJsonGenerator generator = new JiraGroupJsonGenerator();
        JSONObject retour = generator.generate(jiraGroup);

        assertEquals("Jacob", retour.get("name"));
    }
}
