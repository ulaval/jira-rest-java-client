package com.atlassian.jira.rest.client.internal.json;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.atlassian.jira.rest.client.api.domain.TeamMember;

public class TeamMemberJsonParserTest {

	@Test
	public void testParse() throws Exception {
		
		final TeamMemberJsonParser parser = new TeamMemberJsonParser();
		final TeamMember teamMember = parser.parse(ResourceUtil.getJsonObjectFromResource("/json/teamMember/valid.json"));
		
		assertEquals(Long.valueOf(95), teamMember.getId());
		assertTrue(teamMember.isShowDeactivate());
		assertNotNull(teamMember.getMember());
		assertNotNull(teamMember.getMembership());
	}
}
