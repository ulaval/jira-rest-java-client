package com.atlassian.jira.rest.client.internal.json;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.URI;

import org.junit.Test;

import com.atlassian.jira.rest.client.api.domain.TeamUser;

public class TeamUserJsonParserTest {

	@Test
	public void testParse() throws Exception {
		
		final TeamUserJsonParser parser = new TeamUserJsonParser();
		final TeamUser teamUser = parser.parse(ResourceUtil.getJsonObjectFromResource("/json/teamUser/valid.json"));
		
		assertEquals("gumou1", teamUser.getName());
		assertEquals("Moutier, Guillaume", teamUser.getDisplayName());
		assertEquals("gumou1", teamUser.getKey());
		assertTrue(teamUser.isJiraUser());
		
		assertNotNull(teamUser.getAvatarUris());
		assertEquals(new URI("/secure/useravatar?size=xsmall&ownerId=gumou1&avatarId=13390"), teamUser.getAvatarUris().get("16x16"));
		assertEquals(new URI("/secure/useravatar?size=small&ownerId=gumou1&avatarId=13390"), teamUser.getAvatarUris().get("24x24"));
		assertEquals(new URI("/secure/useravatar?size=medium&ownerId=gumou1&avatarId=13390"), teamUser.getAvatarUris().get("32x32"));
		assertEquals(new URI("/secure/useravatar?ownerId=gumou1&avatarId=13390"), teamUser.getAvatarUris().get("48x48"));
	}
}
