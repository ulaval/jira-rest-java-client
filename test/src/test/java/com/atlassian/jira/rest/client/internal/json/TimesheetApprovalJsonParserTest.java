package com.atlassian.jira.rest.client.internal.json;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.atlassian.jira.rest.client.api.domain.Approval;
import com.atlassian.jira.rest.client.api.domain.TimesheetApproval;

public class TimesheetApprovalJsonParserTest {

	@Test
	public void testParse() throws Exception {
		final TimeSheetApprovalJsonParser parser = new TimeSheetApprovalJsonParser();
		final TimesheetApproval timesheetApproval = parser.parse(ResourceUtil.getJsonObjectFromResource("/json/timesheetApproval/valid.json"));
		assertNotNull(timesheetApproval.getPeriod());
		assertNotNull(timesheetApproval.getApprovals());
		for(Approval approval : timesheetApproval.getApprovals()){
			assertNotNull(approval);
		}
	}
	
	@Test
	public void testParseApprovalsEmpty() throws Exception {
		final TimeSheetApprovalJsonParser parser = new TimeSheetApprovalJsonParser();
		final TimesheetApproval timesheetApproval = parser.parse(ResourceUtil.getJsonObjectFromResource("/json/timesheetApproval/approvalsEmpty.json"));
		assertNotNull(timesheetApproval.getPeriod());
		assertNotNull(timesheetApproval.getApprovals());
		assertTrue(timesheetApproval.getApprovals().isEmpty());
	}
}
