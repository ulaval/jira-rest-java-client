package com.atlassian.jira.rest.client.internal.json;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import com.atlassian.jira.rest.client.api.domain.Membership;

public class MembershipJsonParserTest {

	@Test
	public void testParse() throws Exception {
		
		final MembershipJsonParser parser = new MembershipJsonParser();
		final Membership membership = parser.parse(ResourceUtil.getJsonObjectFromResource("/json/membership/valid.json"));
		
		assertEquals(Long.valueOf(95), membership.getId());
		assertNull(membership.getDateFrom());
		assertNull(membership.getDateTo());
		assertEquals(Long.valueOf(100), membership.getAvailability());
		assertEquals(Long.valueOf(95), membership.getTeamMemberId());
		assertEquals(Long.valueOf(62), membership.getTeamId());
		assertEquals("active", membership.getStatus());
		assertNotNull(membership.getRole());
	}
}
