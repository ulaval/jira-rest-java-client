package com.atlassian.jira.rest.client.internal.json;

import static org.junit.Assert.assertEquals;

import org.joda.time.DateTime;
import org.junit.Test;

import com.atlassian.jira.rest.client.api.domain.Period;

public class PeriodJsonParserTest {
	
	@Test
	public void testParse() throws Exception {
		final PeriodJsonParser parser = new PeriodJsonParser();
		final Period period = parser.parse(ResourceUtil.getJsonObjectFromResource("/json/period/valid.json"));
		assertEquals("09032015", period.getPeriodString());
		assertEquals("WEEK", period.getPeriodView());
		assertEquals(new DateTime(2015,3, 9, 0, 0, 0, 0), period.getDateFrom());
		assertEquals(new DateTime(2015,3, 15, 0, 0, 0, 0), period.getDateTo());
	}
}
