package com.atlassian.jira.rest.client.internal.json;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.atlassian.jira.rest.client.api.domain.MembershipRole;

public class MembershipRoleJsonParserTest {

	@Test
	public void testParse() throws Exception {
		
		final MembershipRoleJsonParser parser = new MembershipRoleJsonParser();
		final MembershipRole membershipRole = parser.parse(ResourceUtil.getJsonObjectFromResource("/json/membershipRole/valid.json"));
		
		assertEquals(Long.valueOf(1), membershipRole.getId());
		assertEquals("Membre", membershipRole.getName());
		assertTrue(membershipRole.isDefault());
	}
	
}
