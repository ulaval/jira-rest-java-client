package com.atlassian.jira.rest.client.internal.json.gen;

import static org.junit.Assert.assertEquals;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Test;

import com.atlassian.jira.rest.client.api.domain.WriteUser;

public class WriteUserGeneratorTest {

    @Test
    public void generatorTest() throws JSONException {

        WriteUser writeUser = new WriteUser("isauc4", "Auclair, Israël", "israel.auclair.1@ulaval.ca");

        WriteUserGenerator generator = new WriteUserGenerator();
        JSONObject retour = generator.generate(writeUser);

        assertEquals("isauc4", retour.get("name"));
        assertEquals("Auclair, Israël", retour.get("displayName"));
        assertEquals("israel.auclair.1@ulaval.ca", retour.get("emailAddress"));
    }
}