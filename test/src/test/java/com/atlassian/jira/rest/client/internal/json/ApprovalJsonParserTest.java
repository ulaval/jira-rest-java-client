package com.atlassian.jira.rest.client.internal.json;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.atlassian.jira.rest.client.api.domain.Approval;

public class ApprovalJsonParserTest {
	
	@Test
	public void testParse() throws Exception {
		final ApprovalJsonParser parser = new ApprovalJsonParser();
		final Approval approval = parser.parse(ResourceUtil.getJsonObjectFromResource("/json/approval/valid.json"));
		assertNotNull(approval.getUser());
		assertEquals("jira-api-test", approval.getUser().getName());
		assertEquals("ready-to-submit", approval.getStatus());
		assertEquals(Long.valueOf(0), approval.getWorkedSeconds());
		assertEquals(Long.valueOf(0), approval.getSubmittedSeconds());
		assertEquals(Long.valueOf(0), approval.getRequiredSeconds());
	}

}
