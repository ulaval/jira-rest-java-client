package com.atlassian.jira.rest.client.internal.json;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.URI;

import org.junit.Test;

import com.atlassian.jira.rest.client.api.domain.Member;
import com.atlassian.jira.rest.client.api.domain.MemberType;

public class MemberJsonParserTest {

	@Test
	public void testParse() throws Exception {
		
		final MemberJsonParser parser = new MemberJsonParser();
		final Member member = parser.parse(ResourceUtil.getJsonObjectFromResource("/json/member/valid.json"));
		
		assertEquals("frbre11", member.getName());
		assertEquals(Long.valueOf(95), member.getTeamMemberId());
		assertEquals(MemberType.USER, member.getType());
		assertTrue(member.isActiveInJira());
		assertEquals("Breton, François", member.getDisplayName());
		
		assertNotNull(member.getAvatarUris());
		assertEquals(new URI("/secure/useravatar?size=xsmall&avatarId=10312"), member.getAvatarUris().get("16x16"));
		assertEquals(new URI("/secure/useravatar?size=small&avatarId=10312"), member.getAvatarUris().get("24x24"));
		assertEquals(new URI("/secure/useravatar?size=medium&avatarId=10312"), member.getAvatarUris().get("32x32"));
		assertEquals(new URI("/secure/useravatar?avatarId=10312"), member.getAvatarUris().get("48x48"));
	}
}
