package com.atlassian.jira.rest.client.internal.json;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.atlassian.jira.rest.client.api.domain.Team;

public class TeamJsonParserTest {
	
	@Test
	public void testParse() throws Exception {
		
		final TeamJsonParser parser = new TeamJsonParser();
		final Team team = parser.parse(ResourceUtil.getJsonObjectFromResource("/json/team/valid.json"));
		assertEquals(Long.valueOf(62), team.getId());
		assertEquals("BATI", team.getName());
		assertEquals("gumou1", team.getLead());
		assertEquals("BATI", team.getSummary());
		assertNotNull(team.getLeadUser());
	}
}
